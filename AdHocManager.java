/**
 * 
 */
package artemislitegame;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Brian Penrose
 *
 */
public class AdHocManager {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//////////////////////////////////test Square Class///////////////////////////////////////////////////////
		
		/*
		Square mySquare = new Square("Test Square", "A simple square for testing the class.", 1, new ArrayList<String>());
		
		mySquare.getSojourners().add("player1");
		mySquare.getSojourners().add("player2");
		
		System.out.println(mySquare.display());
		
		mySquare.getSojourners().remove("player1");
		
		System.out.println(mySquare.display());
		*/
		
		//////////////////////////////////test DevSquare Class////////////////////////////////////////////////////
		
		/*
		DevSquare myDevSquare = new DevSquare("Test DevSquare", "A simple developement square for testing the class.", 2, new ArrayList<String>(),
				100, "player1", DevelopementLevel.L0, 1);
		
		myDevSquare.getSojourners().add("player3");
		myDevSquare.getSojourners().add("player4");
		
		System.out.println(myDevSquare.display());
		
		myDevSquare.getSojourners().remove("player4");
		myDevSquare.getSojourners().add("player1");
	
		//change this to see devCost & visitorFee change.
		myDevSquare.setDevLevel(DevelopementLevel.valueOf(myDevSquare.getDevLevel().getLevel() + 3));
		//This incorporates enum methods to ensure that devLevel is 'set' by adding integers to the current enum's value.
		//CAN ONLY USE ENUM VALUES! NUM POINTER EXCEPTION IF OTHERWISE!
		//should be able to handle in Play .develope() method
		
		System.out.println(myDevSquare.display());
		System.out.println("Developement cost: " + myDevSquare.getDevCost());
		System.out.println("Visitor fee: " + myDevSquare.getVisitorFee());
		System.out.println();
		*/
		
		//////////////////////////////////test Player Class////////////////////////////////////////////////////
		
		/*
		Player myPlayer = new Player("Ashley");
		
		System.out.println(myPlayer.display());
		
		//purchasing a square
		myPlayer.setPlayerResources(myPlayer.getPlayerResources()-myDevSquare.getCost());		
		myPlayer.getOwnedSquares().add(2); //2 is the squareID of the test DevSquare.
		
		System.out.println(myPlayer.display());
		
		//this checks through own sqaure array but needs to loop through all squares also and check when both are equal!
		for(Integer squareID : myPlayer.getOwnedSquares()){
			if(squareID.equals(myDevSquare.getSquareID())) {
				System.out.println(myDevSquare.display());
			}else {
				System.out.println("in Loop");
			}
		}
		*/
		
		//////////////////////////////////test Game Class////////////////////////////////////////////////////
		
		
		Game game = new Game();
		Scanner scan = new Scanner(System.in);
		
		System.out.println("number of players: " + game.getNumberOfPlayers());
		game.playerNumber(scan);
		System.out.println("number of players: " + game.getNumberOfPlayers());
		
		for(int counter = 1 ; counter <= game.getNumberOfPlayers(); counter++) {
			game.addPlayer(scan);
		}
		
		game.turnOrderSorter(game.getPlayers());
		
		game.createBoard();
		game.createPlayerMap(game.getPlayers());
		
		System.out.print("Players: ");
		for(Player player : game.getPlayers()) {
			System.out.print(player.getPlayerName() + "[" + player.getPlayerTurnOrderPosition() + "] ");
			game.getBoard().get(1).getSojourners().add(player.getPlayerName());
		}
		
		System.out.println("");
		
		/*
		System.out.println(game.getBoard().keySet());
		for(Square s : game.getBoard().values()) {
			System.out.println(s.display());
		}
		*/
		
		//setting squares as owned by player 2 to check the collect() method
		for(Square square : game.getBoard().values()) {
			if (square instanceof DevSquare) {
				DevSquare s = (DevSquare) square;
				s.setOwner(game.getPlayers().get(1).getPlayerName());
			}
		}
		
		//checking that PlayerList and PlayerMap Players are not the same - they are different objects!
		/*
		System.out.println(game.getPlayers().get(0).getPlayerResources());
		game.getPlayers().get(0).setPlayerResources(game.getPlayers().get(0).getPlayerResources()+100);
		System.out.println(game.getPlayers().get(0).getPlayerResources());
		System.out.println(game.getPlayerMap().get("A").getPlayerResources());
		*/
		
		//System.out.println("Sojourners: " + game.getBoard().get(1).getSojourners().toString());
		
		//sets the current position of player 2 to a DevSquare and sets him as a sojourner...
		game.getPlayers().get(1).setCurrentPosition(2);
		game.getBoard().get(game.getPlayers().get(1).getCurrentPosition()).getSojourners().add(game.getPlayers().get(1).getPlayerName());
		System.out.println("start position " + game.getPlayers().get(1).getPlayerName() + ": " + game.getPlayers().get(1).getCurrentPosition());
		//and sets that as owned by Player 1 to test the .sell() method...
		game.getPlayers().get(0).getOwnedSquares().add(2);
		DevSquare s = (DevSquare) game.getBoard().get(2);
		s.setOwner(game.getPlayers().get(0).getPlayerName());
		
		//setting resources high to test handling of attempt to develope beyond L4, i.e. beyond defined enum!
		game.getPlayers().get(1).setPlayerResources(game.getPlayers().get(1).getPlayerResources()+2000);
		//testing develope method
		game.getPlayers().get(1).develope(game, scan);
		
		System.out.println(game.getBoard().get(2).getName() + " Sojourners: " + game.getBoard().get(2).getSojourners().toString());
		System.out.println(game.getPlayers().get(0).getPlayerName() + " owned Squares: " + game.getPlayers().get(0).getOwnedSquares().toString());
		
		//System.out.println("start position: " + game.getPlayers().get(0).getCurrentPosition());
		game.getPlayers().get(0).move(game, scan);
		//System.out.println("end position: " + game.getPlayers().get(0).getCurrentPosition());
		
		//Selling method call
		game.getPlayers().get(0).sell(game, scan);
		
		//ending resources for players 1 & 2...
		System.out.println("end resources [" + game.getPlayers().get(0).getPlayerName() + "]: "  + game.getPlayers().get(0).getPlayerResources());
		System.out.println("end resources [" + game.getPlayers().get(1).getPlayerName() + "]: "  + game.getPlayers().get(1).getPlayerResources());
		
		System.out.println("Owner " + s.getName() + ": " + s.getOwner());
		
		//testing devDescription after reading description from external .txt file.
		System.out.println(s.getDevDescription());
		s.setDevLevel(DevelopementLevel.L2);
		System.out.println(s.getDevDescription());
		System.out.println(s.getDescription());
				
		scan.close();
		
	}

}