/**
 * 
 */
package artemislitegame;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Brian Penrose
 *
 */
public class Game {

	/**
	 * Squares in the map that are NOT DevSquares.
	 */
	private final int NON_DEV_SQUARE_ONE = 1;
	private final int NON_DEV_SQUARE_TWO = 7;

	private int numberOfPlayers;
	// String required for player turn order
	private List<Player> playerList = new ArrayList<Player>();
	// map required to add and remove players from squares easily.
	private Map<String, Player> playerMap = new HashMap<String, Player>();
	private Map<Integer, Square> board = new HashMap<Integer, Square>();
	private SortedSet<Integer> squareSets = new TreeSet<>();

	/**
	 * Default constructor
	 */
	public Game() {
	}

	/**
	 * @return the numberOfPlayers
	 */
	public int getNumberOfPlayers() {
		return numberOfPlayers;
	}

	/**
	 * Sets the number of players in the game. If less than 2 or greater than 4
	 * 
	 * @param numberOfPlayers the numberOfPlayers to set
	 */
	public void setNumberOfPlayers(int numberOfPlayers) {
		if (numberOfPlayers >= 2 && numberOfPlayers <= 4) {
			this.numberOfPlayers = numberOfPlayers;
		} else {
			System.out.println("Must be between 2 and 4 players. Please enter a valid number...");
		}
	}

	/**
	 * @return the players
	 */
	public List<Player> getPlayers() {
		return playerList;
	}

	/**
	 * @return the playerMap
	 */
	public Map<String, Player> getPlayerMap() {
		return playerMap;
	}

	/**
	 * @return the board
	 */
	public Map<Integer, Square> getBoard() {
		return board;
	}

	/**
	 * @return the squareSets
	 */
	public SortedSet<Integer> getSquareSets() {
		return squareSets;
	}

	/**
	 * Asks the user to input the number of players in the game. This figure is then
	 * set as numberOfPlayers implementing the business rules contained in that
	 * method.
	 * 
	 * @param scan
	 */
	public void playerNumber(Scanner scan) throws InputMismatchException{
		scan.reset();
		while (numberOfPlayers < 2 || numberOfPlayers > 4) {
			try {
				System.out.println("How many people will be playing?");
				int playerCount = scan.nextInt();
				this.setNumberOfPlayers(playerCount);
			} catch (InputMismatchException ime) {
				System.out.println("Please enter an integer value.");
				scan.next();
				this.playerNumber(scan);
			}

		}
	}

	/**
	 * Adds a player to the game. It asks the user to input a name for the player
	 * and if taken a prompt will appear asking the user for a different name until
	 * a unique name has been entered. It then calls the Player constructor using
	 * the name and assigns default positions and resources. Finally it simulates
	 * the rolling of a 6-sided die to determine turn initiative.
	 * 
	 * @param scan
	 */
	public void addPlayer(Scanner scan) {
		scan.reset();
		String name = "";
		int turnOrderValue;
		Player p;
		boolean nameSet = false;
		while (!nameSet) {
			System.out.println("Please enter player name:");
			name = scan.next();
			if (!nameCheck(playerList, name)) {
				nameSet = true;
			} else {
				System.out.println("Name is already taken. Please select another:");
			}
		}
		p = new Player(name);
		playerList.add(p);
		System.out.println("Rolling for turn order...");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
		turnOrderValue = (int) (1 + Math.random() * 6) * this.numberOfPlayers;
		System.out.println("You rolled a " + turnOrderValue);
		p.setPlayerTurnOrderPosition(turnOrderValue);
	}

	/**
	 * Returns a true/false based on whether a name is taken already by a player.
	 * 
	 * @param players
	 * @param name
	 * @return
	 */
	public boolean nameCheck(List<Player> players, String name) {
		boolean nameTaken = false;
		for (Player player : players) {
			if (player.getPlayerName().equals(name)) {
				nameTaken = true;
			}
		}
		return nameTaken;
	}

	/**
	 * Checks the turnOrderValue of each player in the List players and evaluates if
	 * any have the same value. If so a re-roll is conducted and +1 is added to the
	 * winning result, if they are the same again the re-rolling continues until a
	 * winner is found. Due to the nature of the sorter used naturally higher rolls
	 * will never be placed below a naturally lower one in turn order as natural
	 * rolls are incremented by the numberOfPlayers whilst a sorted roll will only
	 * ever be adjusted by 1. As the highest adjustment to a tied roll is when all
	 * four players have the same roll. In this case the highest adjustment is
	 * numberOfPlayers-1. So an adjustment will never push a player into the next
	 * natural increment.
	 * 
	 * @param players
	 */
	public void turnOrderSorter(List<Player> players) {
		int turnOrderValueInner, turnOrderValueOuter;
		Collections.sort(players, new TurnOrderSorter());
		for (Player outer : players) {
			for (Player inner : players) {
				if (!inner.equals(outer) && inner.getPlayerTurnOrderPosition() == outer.getPlayerTurnOrderPosition()) {
					while (inner.getPlayerTurnOrderPosition() == outer.getPlayerTurnOrderPosition()) {
						System.out.println("Players " + inner.getPlayerName() + " and " + outer.getPlayerName()
								+ " have the same turn order value. A re-roll is required...");
						turnOrderValueOuter = (int) (1 + Math.random() * 6);
						turnOrderValueInner = (int) (1 + Math.random() * 6);
						System.out.println(outer.getPlayerName() + " rolled a " + turnOrderValueOuter);
						System.out.println(inner.getPlayerName() + " rolled a " + turnOrderValueInner);
						if (turnOrderValueOuter > turnOrderValueInner) {
							outer.setPlayerTurnOrderPosition(outer.getPlayerTurnOrderPosition() + 1);
						} else if (turnOrderValueOuter < turnOrderValueInner) {
							inner.setPlayerTurnOrderPosition(inner.getPlayerTurnOrderPosition() + 1);
						}
					}
				}
			}
		}
		Collections.sort(players, new TurnOrderSorter());
	}

	/**
	 * Creates a map of the players using their names as keys. This allows easy
	 * access when adding and removing them from squares for move/sell actions.
	 * 
	 * @param playerList
	 */
	public void createPlayerMap(List<Player> playerList) {
		for (Player p : playerList) {
			playerMap.put(p.getPlayerName(), p);
		}
	}

	/**
	 * Creates the board for the game by creating a pre-defined number of squares
	 * and then reading in the details for each from an external .txt file. It adds
	 * the squares to a hashMap using the squareID as the key.
	 */
	public void createBoard() {
		File file = new File("SquareDetails.txt");
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			for (int counter = 1; counter <= 12; counter++) {
				if (counter == NON_DEV_SQUARE_ONE || counter == NON_DEV_SQUARE_TWO) {
					Square s = new Square();
					s.setName(br.readLine()); // name
					s.setDescription(br.readLine()); // description
					s.setSquareID(Integer.parseInt(br.readLine())); // ID
					board.put(s.getSquareID(), s);
					br.readLine();
				} else {
					DevSquare s = new DevSquare();
					s.setName(br.readLine()); // name
					s.setDescription(br.readLine()); // description
					s.setSquareID(Integer.parseInt(br.readLine())); // ID
					s.setCost(Integer.parseInt(br.readLine())); // cost
					s.setSet(Integer.parseInt(br.readLine())); // set that it belongs to.
					s.setDevDescript0(br.readLine());
					s.setDevDescript1(br.readLine());
					s.setDevDescript2(br.readLine());
					s.setDevDescript3(br.readLine());
					s.setDevDescript4(br.readLine());
					board.put(s.getSquareID(), s);
					br.readLine();

				}
			}

			br.close();
			fr.close();

		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		gameSets();

	}

	/**
	 * Creates a sorted set of the possible sets that a DevSquare can belong to.
	 */
	public void gameSets() {
		for (Square s : this.getBoard().values()) {
			if (s instanceof DevSquare) {
				DevSquare sq = (DevSquare) s;
				squareSets.add(sq.getSet());
			}
		}
	}

	/**
	 * Checks that all players in the game have positive resource pools and returns
	 * a boolean indication if this is true or false.
	 * 
	 * @return
	 */
	public boolean resourceCheck() {
		boolean haveResources = true;
		for (Player p : this.playerMap.values()) {
			if (p.getPlayerResources() < 0) {
				haveResources = false;
			} else {
				continue;
			}
		}
		return haveResources;
	}

	/**
	 * Checks if all squares in the current board are fully developed. A boolean is
	 * returned indicating if this is true or false.
	 * 
	 * @return
	 */
	public boolean fullyDeveloped() {
		boolean fullDev = true;
		for (Square s : this.board.values()) {
			if (s instanceof DevSquare) {
				DevSquare ds = (DevSquare) s;
				if (ds.getDevLevel() != DevelopementLevel.L4) {
					fullDev = false;
				} else {
					continue;
				}
			} else {
				continue;
			}
		}
		return fullDev;
	}

}