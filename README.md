# ArtemisLite

Develop a Text Based User Interface Virtual Board Game

This game is based of the famous monoplay board game. There are a maximum of 4 players allowed for this game but it can also be played with just 2. The game was deisnged for a academic project in software engineering. The game uses Java language on the Eclipse platform.

Players will work together in order to develop all the elements on the board to achieve the mission. Players must go in turn by rolling a simulated dice. The number on the dice relates to the number spaces a player will move. If he/she rolls a six they will take another turn. Once a player lands on a square(element) they can choose to develop this element with the money they have in their resources. Players can choose to sell there elements to other players for strategic game play and can also purchase elements of other players. There are 4 levels of development a player has to level up in order to obtain a fully completed element. Once all elements on the board are developed fully then the game is over and all players are successful. If a player goes bust and has no money left then all the players lose the game. A protolog will display each players scores and resouces upon cokmpletion or failure of the game. 
