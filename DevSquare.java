/**
 * 
 */
package artemislitegame;

import java.util.List;

/**
 * @author Brian Penrose
 *
 */
public class DevSquare extends Square implements IDisplay {
	
	private int cost;
	private String owner;
	private int set; //might change to 'colour' or 'field'
	private DevelopementLevel devLevel = DevelopementLevel.L0;
	private int devCost;
	private String devDescription;	
	private String devDescript0;
	private String devDescript1;
	private String devDescript2;
	private String devDescript3;
	private String devDescript4;

	/**
	 * Default Constructors
	 */
	public DevSquare() {}

	/**
	 * Constructor with full arguments 
	 * @param name
	 * @param description
	 * @param squareID
	 * @param sojourners
	 */
	public DevSquare(String name, String description, int squareID, List<String> sojourners, int cost, String owner, 
			DevelopementLevel devLevel, int set) {
		super(name, description, squareID, sojourners);
		this.cost = cost;
		this.owner = owner;
		this.devLevel = devLevel;
		this.set = set;
	}

	/**
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

	/**
	 * @param cost the cost to set
	 */
	public void setCost(int cost) {
		this.cost = cost;
	}

	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * @return the devLevel
	 */
	public DevelopementLevel getDevLevel() {
		return devLevel;
	}

	/**
	 * @param devLevel the devLevel to set
	 */
	public void setDevLevel(DevelopementLevel devLevel) {
		try {
			if(devLevel.isDefinedEnum(devLevel)) {
				this.devLevel = devLevel;
			}
		}catch(NullPointerException npe) {
			System.out.println("Enum must be between levels 0 and 4!\n");
		}
	}

	/**
	 * @return the devCost
	 */
	public int getDevCost() {
		if(this.getDevLevel().getLevel()<DevelopementLevel.L3.getLevel()) {
			this.devCost = this.cost;
		}else if(this.getDevLevel().getLevel()<DevelopementLevel.L4.getLevel()) {
			this.devCost = 3 * this.cost;
		}else {
			System.out.println("Cannot be developed further!");
		}
		return devCost;
	}

	/**
	 * @param devCost the devCost to set
	 */
	public void setDevCost(int devCost) {
	}

	/**
	 * @return the visitorFee
	 */
	public int getVisitorFee() {		
		return this.getCost()/10 * (this.getDevLevel().getLevel() + 1);
	}

	/**
	 * @param visitorFee the visitorFee to set
	 */
	public void setVisitorFee(int visitorFee) {
	}

	/**
	 * @return the set
	 */
	public int getSet() {
		return set;
	}

	/**
	 * @param set the set to set
	 */
	public void setSet(int set) {
		this.set = set;
	}

	/**
	 * @return the devDescription
	 */
	public String getDevDescription() {
		if(this.getDevLevel().getLevel() == 0) {
			this.devDescription = devDescript0;
		}else if(this.getDevLevel().getLevel() == 1) {
			this.devDescription = devDescript1;
		}else if(this.getDevLevel().getLevel() == 2) {
			this.devDescription = devDescript2;
		}else if(this.getDevLevel().getLevel() == 3) {
			this.devDescription = devDescript3;
		}else if(this.getDevLevel().getLevel() == 4) {
			this.devDescription = devDescript4;
		}
		return devDescription;
	}
	
	/**
	 * @param devDescript0 the devDescript0 to set
	 */
	public void setDevDescript0(String devDescript0) {
		this.devDescript0 = devDescript0;
	}

	/**
	 * @param devDescript1 the devDescript1 to set
	 */
	public void setDevDescript1(String devDescript1) {
		this.devDescript1 = devDescript1;
	}

	/**
	 * @param devDescript2 the devDescript2 to set
	 */
	public void setDevDescript2(String devDescript2) {
		this.devDescript2 = devDescript2;
	}

	/**
	 * @param devDescript3 the devDescript3 to set
	 */
	public void setDevDescript3(String devDescript3) {
		this.devDescript3 = devDescript3;
	}

	/**
	 * @param devDescript4 the devDescript4 to set
	 */
	public void setDevDescript4(String devDescript4) {
		this.devDescript4 = devDescript4;
	}

	@Override
	public String display() {
		return "Name: " + this.getName() + "\nDescription: " + this.getDescription() + "\nOwner: " + this.owner + "\nCost: " 
				+ this.cost + "\nDevelopement Level: " + this.devLevel + "\nDevelopement Description: " + this.getDevDescription() 
				+ "\nVistor's Fee: " + this.getVisitorFee() + "\nVisitors: " + this.getSojourners().toString();
	}

}
